---
title: "Configurer son poste pour utiliser `{datalibaba}`"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{tuto_config}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```


```{r setup}
library(datalibaba)
```

## Principes 

Afin de renseigner ses identifiants de connexion une fois pour toute (sur un poste de travail) et de ne pas les faire apparaître en clair dans les scripts de préparation de données, le package `{datalibaba}` vous propose de saisir ces informations dans votre fichier de variables d'environnement R, intitulé .Renviron.


Les variables d'environnement utilisées par `{datalibaba}` doivent être de la forme :
```
server=adresse_ip
user_does=identifiant
pwd_does=lemotdepassesansquote
port=lenumerodeport 

```
Il ne faut pas mettre entre '' ou "" la valeur de ces variables.

Le suffixe `does` du nom d'utilisateur et du mot de passe peut être remplacé par n'importe quel suffixe permettant d'identifier le rôle de connexion ou autre, par exemple, `user_dreal` et `pwd_dreal`. 
Mais en l'état des développements actuels, la fonction `check_server_enriron()`, qui vérifie la configuration de votre poste ne les reconnaîtra pas comme valides alors qu'ils le sont.

## Procédure pas à pas

#### 1ère étape

Ouvrir le fichier de variables d'environnement R (.Renviron) avec  :
```{r, eval = FALSE}
# install.packages("usethis")
usethis::edit_r_environ()
```
Dans Rstudio, ce fichier s'ouvre dans le cadran dévolu à l'édition des scripts.

#### 2e étape  
Saisir les 4 variables d'environnement nécessaires à la connexion sur ce fichier texte. 
Cette saisie doit prendre la forme :   
```
 server=11.22.333.444  
 user_does=utilisateur  
 pwd_does=lemotdepassesansquote  
 port=5432  
```

#### 3e étape
Enregistrer le fichier .Renviron, le fermer, redémarrer la session R pour que ces nouvelles variables soit lues. 
La configuration est terminée. 
`{datalibaba}` peut maintenant être utilisé.

## Usage 

#### Utilisateur suffixé "_does"

Dans le cas d'identifiants de connexion déclarés avec le suffixe '_does', il n'est plus nécessaire de préciser ensuite quels identifiants de connexion il faut utiliser. 
Par défaut les fonctions de connexion, de lecture ou d'écriture de données vont silencieusement utiliser `user_does` et `pwd_does`.

#### Autres utilisateurs
Dans les autres cas, il faudra préciser les identifiants de connexion à utiliser avec le paramètre `user`, par exemple, les fonctions : 
```{r, eval = FALSE}
poster_data(data = iris, table = "test_iris", schema = "public", db = "public", pk = NULL, 
            post_row_name = FALSE, overwrite = TRUE, user = "dema")

importer_data(table = "test_iris", schema = "public", db = "public", user = "dema")
```

vont aller chercher les identifiants / mot de passe dans le .Renviron déclarés au niveau des variables d'environnement `user_dema` et `pwd_dema`.

#### Plusieurs serveurs ou port de base de données
Les fonctions de {datalibaba} utilisent par défaut, et silencieusement, les variables d'environnement `server`, `user_does`, `pwd_does`, `port`. 

Si l'utilisateur souhaite recourir à un serveur autre que celui renseigné au niveau de la variable d'environnement `serveur` ou utiliser un autre port que celui par défaut, il peut créer de nouvelles variables d'environnement dans le .Renviron avec la procédure détaillée ci dessus et les appeler grâce à la fonction `Sys.getenv("server_local")`.

Voici un exemple de configuration secondaire : 

Ajouts au fichier .Renviron :    
```
server_local=127.0.0.1 
user_local=admin  
pwd_local=lemotdepassesansquote  
```  

Utilisation dans un scripts de datapréparation

```{r, eval = FALSE}
poster_data(data = iris, table = "test_iris", schema = "public", db = "public", pk = NULL, overwrite = TRUE, 
            post_row_name = FALSE, user = "local", server = Sys.getenv("server_local"))

importer_data(table = "test_iris", schema = "public", db = "public", 
              user = "local", server = Sys.getenv("server_local"))
```



