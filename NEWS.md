# datalibaba 0.0.0.9004
- Meilleure gestion des facteurs ( encodage + échappement du caractère spécial " dans les levels)

# datalibaba 0.0.0.9003
- Correction d'un bug dans `poster_data()`, la fonction plantait si les modalités d'un facteur contenaient des caractères spéciaux.
- Amélioration de la documentation sur la configuration du .Renviron.

# datalibaba 0.0.0.9002
- Correction d'un bug dans la fonction `importer_data()` : nommage du champ géo tel qu'il avait été chargé

# datalibaba 0.0.0.9001
- Correction d'un bug dans la fonction `poster_data()` : paramétrage des contraintes géo selon le nom de la colonne géo. (auparavant utilisation de 'geometry' pour toutes les tables, ce qui provoquait des plantages).  
- Ajout de message pour le post de dataset géo afin d'identifier quelle étape est en cours.

# datalibaba 0.0.0.9000

- ajout de la fonction `check_server_renviron()` qui vérifie si les variables système sont bien activées via .Renviron. 
- ajout de la fonction `connect_to_db()` : Connexion au serveur de base de données. 
- ajout de la fonction `list_schemas()` : Lister les schémas sur un connecteur. 
- ajout de la fonction `list_tables()` : Lister les tables sur un schéma d'un connecteur. 
- ajout de la fonction `standard_var_pg()` pour normaliser le nom des champs en vue de leur versement sur le SGBD
- ajout des fonctions `post_data()`, `post_data_dbi()`, `poster_data()` : Versement d'un dataset sur le serveur. 
- ajout des fonctions `get_data()`,  `get_data_dbi()`,  `importer_data()` : Import d'un dataset du serveur. 
- ajout des fonctions `commenter_table()`, `commenter_schema()`, `commenter_champs()` et `post_dico_attr()` : Rédaction de commentaires descriptifs sur les schémas et les tables du serveur de données. 
- ajout des fonctions `get_table_comments()` et `transferer_table_comment()` : qui récupèrent et transfèrent les commentaires descriptifs d'une table du serveur de données à une autre. 
- ajout de la fonction `exporter_table_comments()` qui récupère le descriptif d'une table SGBD (commentaire de table et commentaires de champs) et les exporte dans un tableur csv.
- ajout fonction `set_schema_rights()` qui affecte à une table les droits de lecture/écriture de son schéma.

* Added a `NEWS.md` file to track changes to the package.
