#' Lister les tables sur un schéma d'un connecteur.
#'
#' @param con Le connecteur de classe PosgreSQLConnection, peut etre NULL si db et user sont tous deux renseignes, sera utilise par defaut si les 3 arguments renseigne.
#' @param db Le nom de la base de donnees a laquelle se connecter pour trouver le schema, si con n'est pas renseigne.
#' @param user Le nom de l'utilisateur de la connexion au serveur de donnees, si con n'est pas renseigne.
#' @param schema Le nom du schéma sur le serveur de données.
#' @return Un vecteur de caractères avec les tables présentes sur le schéma schema du connecteur con.
#' @importFrom attempt stop_if_all message_if_none stop_if stop_if_not
#' @importFrom DBI dbGetQuery dbDisconnect
#' @importFrom glue glue
#' @export
#'
#' @examples
#' \dontrun{
#' list_tables(con = con, schema = "conjoncture")
#' list_tables(schema = "conjoncture", db = "datamart", user = "does")
#' }
list_tables <- function(con = NULL, schema = NULL, db = NULL, user = NULL) {
  connexion <- con
  attempt::stop_if_all(c(connexion, db, user), is.null, msg = "con, db et user ne sont pas renseign\u00e9s, merci de fournir les arguments necessaires a la connexion (soit con, soit db + user). ")
  attempt::stop_if_all(c(connexion, db), is.null, msg = "con et db ne sont pas renseign\u00e9s, merci de fournir les arguments necessaires a la connexion (soit con, soit db + user). ")
  attempt::stop_if_all(c(connexion, user), is.null, msg = "con et user ne sont pas renseign\u00e9s, merci de fournir les arguments necessaires a la connexion (soit con, soit db + user). ")
  attempt::stop_if(schema, is.null, msg = "schema n'est pas renseign\u00e9. ")
  # attempt::message_if_none(c(connexion, db, user), is.null, msg = "con, db et user sont tous renseign\u00e9s, seul con sera utilise pour la connexion. ")

  con_a_creer_fermer <- FALSE # initialisation
  if(is.null(connexion)) {
    con_a_creer_fermer <- TRUE
    connexion <- connect_to_db(db, user)
  }

  attempt::stop_if_not(schema, ~ .x %in% list_schemas(connexion), msg = glue::glue("{schema} n'existe pas. "))

  all_tables <- DBI::dbGetQuery(conn = connexion,
                                statement = glue::glue("SELECT table_name FROM information_schema.tables
                   WHERE table_schema='{schema}'"))

  if(con_a_creer_fermer) {
    DBI::dbDisconnect(connexion)
  }

  return(all_tables$table_name)

}


