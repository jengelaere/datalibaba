
#' set_schema_rights() permet d'actualiser les privileges d'une ou de toutes les tables d'un schema a partir des privileges definis au niveau du schema
#'
#' @param schema le nom du schema pour lequel on veut tranmettre les privilèges par defaut aux tables qu'il contient
#' @param table le nom de la table qui va heriter des privilèges par defaut du schema, laisser à NULL pour agir sur toutes les tables
#' @param con la connexion a la base qui contient le schema, laisser à NULL pour l'établir a partir des identifiant de connexions db, user serveur
#' @param db si con est null, le nom de la base de donnees a laquelle se connecter
#' @param user le nom de l'utilisateur qui se connecte
#' @param server l'adresse ip du server auquel se connecter, laisser a NULL pour utiliser l'adresse IP definie dans le .Renviron.
#'
#' @return NULL
#' @export
#' @importFrom attempt stop_if_all stop_if
#' @importFrom DBI dbGetQuery
#' @importFrom dplyr transmute summarise pull select distinct case_when
#' @importFrom purrr map2_chr
#' @importFrom rlang .data
#' @importFrom tibble tribble enframe
#' @importFrom tidyr separate
#'
#' @examples
#' \dontrun{
#' set_schema_rights(schema = "public", table = "opah_r52", db  = "public", user = "does")
#' }

set_schema_rights <- function(schema = "public", table = NULL, con = NULL, db  = "public", user = "does", server = NULL) {
  connexion <- con
  attempt::stop_if_all(c(connexion, db, user), is.null, msg = "con, db et user ne sont pas renseign\u00e9s, merci de fournir les arguments necessaires a la connexion (soit con, soit db + user). ")
  attempt::stop_if_all(c(connexion, db), is.null, msg = "con et db ne sont pas renseign\u00e9s, merci de fournir les arguments necessaires a la connexion (soit con, soit db + user). ")
  attempt::stop_if_all(c(connexion, user), is.null, msg = "con et user ne sont pas renseign\u00e9s, merci de fournir les arguments necessaires a la connexion (soit con, soit db + user). ")
  attempt::stop_if(schema, is.null, msg = "schema n'est pas renseign\u00e9. ")

  if(is.null(connexion)) { con <- connect_to_db(db = db, user = user, server = server) }

  # recuperartion de la liste des privileges par defaut du schema
  query <- paste0("SELECT nspname::varchar AS nom_schema, defaclobjtype::varchar AS type_obj, defaclacl::varchar AS liste_priv FROM pg_default_acl a JOIN pg_namespace b ON a.defaclnamespace=b.oid WHERE nspname = '", schema,"' AND defaclobjtype = 'r';")
  droits <- DBI::dbGetQuery(conn = con, statement = query)

  # referentiel de passage entre l'abreviation et son privilege
  ref_privilege <- tibble::tribble(
    ~verbe,     ~abrv,
    "SELECT",     "r",
    "INSERT",     "a",
    "UPDATE",     "w",
    "DELETE",     "d",
    "TRUNCATE",	  "D",
    "REFERENCES", "x",
    "TRIGGER",	  "t"
  )

  tables_concernees <- ifelse(is.null(table), "ALL TABLES", table)

  droit2 <- dplyr::transmute(droits, liste_priv = gsub("\\{|\\}", "", .data$liste_priv) %>% strsplit(split = ",") %>% list) %>%
    dplyr::summarise(liste_priv = list(unlist(.data$liste_priv)) ) %>%
    dplyr::pull(.data$liste_priv) %>% unlist() %>% tibble::enframe(name = NULL) %>%
    tidyr::separate(col = .data$value, sep = "([=/])", into = c("role", "droits", "granter"), remove = TRUE) %>%
    dplyr::select(-.data$granter) %>%
    dplyr::distinct() %>%
    mutate(privilege = dplyr::case_when(
      .data$droits == "arwdDxt" ~ "ALL",
      .data$droits == "r" ~ "SELECT",
      TRUE ~ purrr::map2_chr(.x = ref_privilege$abrv, .y = ref_privilege$verbe, .f = ~ ifelse(grepl(pattern = .x, .data$droits), .y, NA_character_)) %>%
        setdiff(., NA_character_) %>%
        paste(collapse = ", ")
    ),
      sql = paste0("GRANT ", .data$privilege," ON ", tables_concernees ," IN SCHEMA ", schema ," TO ", .data$role, ";")
    )


}

